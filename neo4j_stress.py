from multiprocessing import Pool
from os import sched_getaffinity
from functools import partial

import click
from neo4j import GraphDatabase

def query(query, f=None):
    def go(tx):
        res = tx.run(query)
        if f:
            for r in res:
                f(r)
        else:
            res.consume()
    return go

def repeat_query(uri, user, password, n, q):
    go = query(q)
    driver = GraphDatabase.driver(uri, auth=(user, password))
    with driver.session() as session:
        for i in range(n):
            session.write_transaction(go)
    driver.close()

@click.command()
@click.option('-u', '--neo4j-user', default="neo4j", help='Neo4j user')
@click.option('-p', '--neo4j-password', default="neo4j", help='Neo4j password')
@click.option('--neo4j-uri', default="bolt://localhost:7687", help="Neo4j URI")
@click.option('-n', '--repetitions', default=100, type=int,
              help="Number of times to run each query")
@click.option('-b', '--before-query', multiple=True, default=[],
              help="Query to run before repeated queries (may be specified multiple times)")
@click.option('-a', '--after-query', multiple=True, default=[],
              help="Query to run after repeated queries (may be specified multiple times)")
@click.argument('queries', nargs=-1)
def cli(neo4j_user, neo4j_password, neo4j_uri, repetitions,
        before_query, after_query, queries):
    "Run QUERIES repeatedly in parallel."
    click.echo('Neo4j stress test')
    nqueries = len(queries)
    ncpus = len(sched_getaffinity(0))  # number of CPUs process can use
    if nqueries > ncpus:
        raise Exception("Can't execute {} queries in parallel, only {}".format(nqueries, ncpus))
    f = partial(repeat_query, neo4j_uri, neo4j_user, neo4j_password)
    driver = GraphDatabase.driver(neo4j_uri, auth=(neo4j_user, neo4j_password))
    with driver.session() as session:
        for q in before_query:
            r = session.write_transaction(query(q))
        if queries:
            # each parallel process must have its own driver/session
            go = partial(f, repetitions)
            with Pool(nqueries) as p:
                p.map(go, queries)
        for q in after_query:
            res = session.write_transaction(query(q, print))
    driver.close()
