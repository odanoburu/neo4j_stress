from setuptools import setup

setup(
    name='neo4j_stress',
    version='0.1',
    py_modules=['neo4j_stress'],
    install_requires=[
        'Click',
        'neo4j'
    ],
    entry_points='''
        [console_scripts]
        neo4j_stress=neo4j_stress:cli
    ''',
)
